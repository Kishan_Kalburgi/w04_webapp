//  We've created an App object (a set of key value pairs) to hold the applcation code.
//  The App object shows how to create a JavaScript object and includes
//  examples of standard programming constructs in JavaScript. 
//  The goal is provide many useful examples in a single code file. 
//
//  When you modify the application, use different ids for your HTML elements.
//  Do not use length and width. 
$('#main_div').hide();
var App = {
  launch: function () {
    App.getNameOrg();
    App.getSportsName();
    App.getRowsNo();
    App.getNoSeats();
    App.computeSeats();
    App.getSeats();
    App.displayExploreButtons();
    App.showExample();
  },
  getNameOrg: function () {
    let answer = prompt("Name of the organization attending the event?", "Goa Club");
    if (answer != null) {
      $("#org_name").html(answer); // $ = jQuery object; in jQuery use # with id, . with class
    }
  },
  getSportsName: function () {
    let answer = prompt("Type of sporting event (e.g. football, soccer, volleyball)?", "Rugby");
    if (answer != null) {
      //document.getElementById("last").innerHTML = answer;
      $("#sport_name").html(answer);  // passing in the inner html with the  jQuery html() method 
    }
  },
  getRowsNo: function () {
    let answer = prompt("Number of rows you want to reserve?", 5);
    if (answer != null) {
      //document.getElementById("width").innerHTML = answer;
      $('#no_rows').html(answer);   // either double or single tick marks designate strings
    }
  },
  getNoSeats: function () {
    let answer = prompt("Number of seats per row you want reserve?", 7);
    if (answer != null) {
      $('#no_seats').html(answer);  // html method works as a getter and a setter
    }
  },
  computeSeats: function () {
    //let inputWidth = parseFloat(document.getElementById("width").innerHTML);
    //let inputLength = parseFloat(document.getElementById("length").innerHTML);
    //let answer = Area.calculateArea(inputWidth, inputLength); // do some checks on the inputs
    //document.getElementById("area").innerHTML = answer;
    let inputWidth = parseFloat($('#no_rows').html());
    let inputLength = parseFloat($('#no_seats').html());
    let answer = App.calculateArea(inputWidth, inputLength); // do some checks on the inputs
    $("#total_seats").html(answer);
    $(".displayText").css('display', 'inline-block');  //overwrites display: hidden to make it visible 
    alert("You have about " + answer + " seats.");
  },
  calculateArea: function (givenWidth, givenLength) {
    if (typeof givenWidth !== 'number' || typeof givenLength !== 'number') {
      throw Error('The given argument is not a number');
    }

    const minWidth = 1;
    const minLength = 1;
    const maxWidth = 100;
    const maxLength = 100;

    // check the first org_name.................
    let width  // undefined
    if (givenWidth < minWidth) {
      width = minWidth;
    }
    else if (givenWidth > maxWidth) {
      width = maxWidth;
    }
    else {
      width = givenWidth;
    }

    //check the second argument ...................
    if (givenLength < minLength) {
      length = minLength;
    }
    else if (givenLength > maxLength) {
      length = maxLength;
    }
    else {
      length = givenLength;
    }

    // calculate the answer and store in a local variable so we can watch the value
    let area = width * length;
    // return the result of our calculation to the calling function
    return area;
  },
  getSeats: function () {
    let area = parseFloat(document.getElementById("total_seats").innerHTML);
    let ct;
    if (area < 1) { ct = 0; }
    else { ct = area }; // estimate 1 per square mile
    // document.getElementById("count").innerHTML = count;
    $("#count").html(ct);
    alert("You could have about " + ct + " seats.");
    $("#count").css("color", "blue");
    $("#count").css("background-color", "yellow");
  },
  showExample: function () {
    document.getElementById("displayPlace").innerHTML = "";
    let totalCount = parseFloat($("#count").html());
    for (var i = 0; i < totalCount; i++) {
      App.addImage(i);
    }
    $('#main_div').show();
  },
  addImage: function (icount) {
    var imageElement = document.createElement("img");
    imageElement.id = "image" + icount;
    imageElement.class = "picture";
    imageElement.style.maxWidth = "90px";
    var displayElement = document.getElementById("displayPlace");
    displayElement.appendChild(imageElement);
    document.getElementById("image" + icount).src = "59-images-of-baby-lamb-clipart-you-can-use-these-free-cliparts-for-sEfudv-clipart.jpg";
  },
  displayExploreButtons: function () {
    $(".displayExploreButtons").css('display', 'block');  //overwrites display: hidden to make it visible 
  },
  exploreHtml: function () {
    alert("Would you like to learn more? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the Elements tab.\n\n" +
      "Hit CTRL-F and search for displayPlace to see the new image elements you added to the page.\n")
  },
  exploreCode: function () {
    alert("Would you like explore the running code? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the top-level Sources tab.\n\n" +
      "In the window on the left, click on the .js file.\n\n" +
      "In the window in the center, click on the line number of the App.getFirstNameorg_name call to set a breakpoint.\n\n" +
      "Click on it again to remove the breakpoint, and one more time to turn it back on.\n\n" +
      "Up on the web page, click the main button to launch the app.\n\n" +
      "Execution of the code will stop on your breakpoint.\n\n" +
      "Hit F11 to step into the App.getFirstNameorg_name function.\n" +
      "Hit F10 to step over the next function call.\n\n" +
      "As you hit F11 and step through your code, the values of local variables appear beside your code - very helpful in debugging.\n\n" +
      "Caution: Hitting F11 in VS Code will make your top-level menu disapper. Hit F11 again to bring it back.\n"
    )
  }
};

